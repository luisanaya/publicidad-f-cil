<?php
/*
Plugin Name: Publicidad fácil
Description: Permite coloca publicidad en tu blog de manera fácil con un widget
Version: 0.1
Author: Luis Anaya | La Élite Web
Author URI: http://laeliteweb.com
License: MIT
*/

add_action( 'widgets_init', 'lew_register_widgets' );
function lew_register_widgets() {
	register_widget('lew_advertising');
}

if (!class_exists('lew_advertising')) {
	class lew_advertising extends WP_Widget {
		function lew_advertising() {
			$widget_ops = array('classname' => 'lew_advertising', 'description' => 'Muestra publicidad en tu sitio web de Wordpress');
			$control_ops = array('width' => 300, 'height' => 350);
			$this->WP_Widget('lew_advertising', 'Publicidad Fácil', $widget_ops, $control_ops);
			
			//Se agrega funcionalidad para el admin de wordpress, encolamos el js que se ha preparado
			add_action( 'admin_enqueue_scripts', array($this, 'enqueue_media_wp_advertising') );
			add_action( 'admin_enqueue_scripts', array($this, 'js_to_widget_advertising') );
			add_action( 'wp_ajax_thumbnail', array($this, 'thumbnail') );
		}

		function widget($args, $instance) {
			$args = array(
				'image_id' => $instance['image_id'],
				'url' => $instance['url'],
				'title' => $instance['title'],
				'target' => $instance['target'],
				'description' => $instance['description'],
				'enlazado' => $instance['enlazado']
			);
			
			if( !empty($args['url']) ){
				echo '
				<div class="widget lew-widget lew-blog-box lew-widget-advertising">
					<div class="custom_widget">
						<h2 class="lew-blog-title">'.$args['title'].'</h2>';
						if( !empty( $args['description'] ) ){
							if( $args['enlazado'] == 'yes' ){
								echo '<a href="'.$args['url'].'" target="'.$args['target'].'"><p>'.$args['description'].'</p></a>';
							} else {
								echo '<p>'.$args['description'].'</p>';
							}
						}
						if( !empty( $args['image_id'] ) ){
							$img = wp_get_attachment_image_src($args['image_id'], 'medium');
							echo '<a href="'.$args['url'].'" target="'.$args['target'].'"><img style="display:block; margin:0 auto;" src="'.$img[0].'" /></a>';
						}
					echo '</div>
				</div>';
			}
			wp_reset_postdata();
			wp_reset_query();
		}
		
		function form($instance) {
			$instance = wp_parse_args((array) $instance, array(
				'image_id' => '',
				'url' => '',
				'title' => '',
				'target' => '_blank',
				'description' => '',
				'enlazado' => 'no'
			));
			$type['image_id'] = strip_tags($instance['image_id']);
			$type['url'] = strip_tags($instance['url']);
			$type['title'] = strip_tags($instance['title']);
			$type['target'] = strip_tags($instance['target']);
			$type['description'] = antiXSS($instance['description']);
			$type['enlazado'] = strip_tags($instance['enlazado']);
			?>
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>"><?php echo __('Titulo Widget', 'lew_widget'); ?>:</label>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo $type['title']; ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('url'); ?>"><?php echo __('Publicidad URL', 'lew_widget'); ?>:</label>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id('url'); ?>" name="<?php echo $this->get_field_name('url'); ?>"  value="<?php echo $type['url']; ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('url'); ?>"><?php echo __('Target', 'lew_widget'); ?>:</label>
				<select class="widefat" id="<?php echo $this->get_field_id('target'); ?>" name="<?php echo $this->get_field_name('target'); ?>">
					<option value="_blank" <?php selected($type['target'],'_blank'); ?>>Página nueva (_blank)</option>
					<option value="_self" <?php selected($type['target'],'_self'); ?>>Misma página (_self)</option>
				</select>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('description'); ?>"><?php echo __('Descripción (opcional)', 'lew_widget'); ?>:</label>
				<textarea class="widefat" cols="20" rows="6" id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>"><?php echo $type['description']; ?></textarea>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('enlazado'); ?>"><?php echo __('¿Titulo enlazado?', 'lew_widget'); ?>:</label>
				<select class="widefat" id="<?php echo $this->get_field_id('enlazado'); ?>" name="<?php echo $this->get_field_name('enlazado'); ?>">
					<option value="no" <?php selected($type['enlazado'],'no'); ?>>No</option>
					<option value="yes" <?php selected($type['enlazado'],'yes'); ?>>Si</option>
				</select>
			</p>
			<div>
				<label for="<?php echo $this->get_field_id('image_id'); ?>"><?php echo __('Selecciona imagen desde Media', 'lew_widget'); ?>:</label><br>
				<input type="hidden" value="<?php echo $type['image_id']; ?>" class="regular-text process_custom_images" id="<?php echo $this->get_field_id('url'); ?>" name="<?php echo $this->get_field_name('image_id'); ?>" max="" min="1" step="1">
				<button class="set_custom_images button">Seleccionar Multimedia</button>
				<div class="sky_advertising_preview">
					<?php
					$imgid = (isset( $type['image_id'] )) ? $type['image_id'] : "";
					$img = wp_get_attachment_image_src($imgid, 'medium');
					?>
					<img src="<?php echo $img[0]; ?>" width="125px" />
				</div>
			</div>
			<?php
		}
		
		function update($new_instance, $old_instance) {
			$instance = $old_instance;
			$instance['title'] = strip_tags($new_instance['title']);
			$instance['url'] = strip_tags($new_instance['url']);
			$instance['target'] = strip_tags($new_instance['target']);
			$instance['description'] = antiXSS($new_instance['description']);
			$instance['image_id'] = strip_tags($new_instance['image_id']);
			$instance['enlazado'] = strip_tags($new_instance['enlazado']);
			return $instance;
		}
		
		function js_to_widget_advertising(){
			//Encolamos jquery, en un principio no detectaba jquery, opte por encolar un cdn
			?>
			<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
			<script>
			/*
			* Developed by Luis Anaya
			* www.luisanaya.com
			*/
			jQuery(document).ready(function(e) {
				if (jQuery('.set_custom_images').length > 0) {
					if ( typeof wp !== 'undefined' && wp.media) {//Image has be selected
						jQuery('body').delegate('.set_custom_images','click',function(e) {//We extended the scope for new elements work at the moment to be activated
							e.preventDefault();
							var button = jQuery(this);
							var id = button.prev();
							wp.media.editor.send.attachment = function(props, attachment) {
								id.val(attachment.id);
								jQuery.ajax({//Do a Ajax request for show the tumbnail
									type: "POST",
									url: "<?php echo admin_url('admin-ajax.php'); ?>",//Send the requeest to admin ajax URL
									data: {id:attachment.id, action:"lew_get_image_ajax"},
									success: function(data){
										button.next().html(data);
									}
								});
							};
							wp.media.editor.open(button);
							return false;
						});
					}
				}
			});
			</script>
			<?php
		}
		
		/*
		* Activamos la opcion de encolar archivos de multimedia
		*/
		function enqueue_media_wp_advertising() {
			if (is_admin ()){ 
				wp_enqueue_media();
			}
		}
		
		/*
		* Obtenemos la imagen cuando ya se ha guardado
		*/
		function get_image($imgid){
			$img = wp_get_attachment_image_src($imgid, 'medium');
			echo '<img src="'.$img[0].'" width="125px" />';
		}
		
		/*
		* Mostramos la imagen haciendo uso de la funcion get_image
		*/
		function thumbnail() {
			$imgid = strip_tags($_REQUEST['id']);
			self::get_image($imgid);
		}
		
	}
	
}

/*
* Con esta función ajax devolveremos la miniatura de la imagen que se está asignando en vivo
*/
function lew_get_image_ajax(){
	$imgid = strip_tags($_REQUEST['id']);
	$img = wp_get_attachment_image_src($imgid, 'full');
	echo '<img src="'.$img[0].'" width="125px" />';
	die();
}
add_action('wp_ajax_lew_get_image_ajax','lew_get_image_ajax');